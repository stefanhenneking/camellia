//
// © 2016-2018 UChicago Argonne.  For licensing details, see LICENSE-Camellia in the licenses directory.
//

#include "ExpFunction.h"
#include "GDAMinimumRule.h"
#include "HDF5Exporter.h"
#include "MeshFactory.h"
#include "ParameterFunction.h"
#include "HeatFormulation.h"
#include "RHS.h"
#include "Solution.h"
#include "TrigFunctions.h"

using namespace Camellia;
using namespace std;

enum ProblemChoice
{
  SINUSOIDAL_MANUFACTURED  // Example 4.1 from Führer and Heuer
};

enum FormulationChoice
{
  GALERKIN,
  PRIMAL,
  CONFORMING_ULTRAWEAK,
  NONCONFORMING_ULTRAWEAK
};

enum NormChoice
{
  GRAPH_NORM,
  MANUAL_GRAPH_NORM
};

const double PI = 3.141592653589793238462;
void runSolve(vector<vector<double>> domainDim, int numElements, double dt, double finalTime, int polyOrder, int deltaP, ProblemChoice problemChoice, FormulationChoice formulationChoice, NormChoice normChoice, bool vis)
{
  int rank = Teuchos::GlobalMPISession::getRank();
  int spaceDim = domainDim.size();
  vector<double> x0(spaceDim);
  vector<double> domainSize(spaceDim);
  vector<int> elementCounts(spaceDim);
  for (int d=0; d<spaceDim; d++)
  {
    x0[d] = domainDim[d][0];
    domainSize[d] = domainDim[d][1] - x0[d];
    elementCounts[d] = numElements;
  }
  
  bool conformingTraces = true;
  if (formulationChoice == CONFORMING_ULTRAWEAK) {
    conformingTraces = true;
    if (rank==0) cout << "Using conforming traces.\n";
  }
  else if (formulationChoice == NONCONFORMING_ULTRAWEAK) {
    conformingTraces = false; // use L^2 traces even for traces of H^1
    if (rank==0) cout << "Using non-conforming traces.\n";
  }
  
  double theta = 1.0; // time-stepping parameter (1=BackwardEuler,0.5=CrankNicolson,0=ForwardEuler)
  HeatFormulation::HeatFormulationChoice choice;
  switch (formulationChoice) {
    case GALERKIN               : choice = HeatFormulation::CONTINUOUS_GALERKIN; break;
    case PRIMAL                 : choice = HeatFormulation::PRIMAL             ; break;
    case CONFORMING_ULTRAWEAK   : choice = HeatFormulation::ULTRAWEAK          ; break;
    case NONCONFORMING_ULTRAWEAK: choice = HeatFormulation::ULTRAWEAK          ; break;
  }
  HeatFormulation form(spaceDim, conformingTraces, dt, theta, choice);
  VarPtr u,u_hat,v,tau;
  u = form.u();
  v = form.v();
  if (choice == HeatFormulation::ULTRAWEAK) {
    u_hat = form.u_hat();
    tau = form.tau();
  }
  BFPtr bf = form.bf();
  if (rank==0) cout << "Number of trial vars: " << bf->varFactory()->trialVars().size() << endl;
  
  FunctionPtr u_exact; // defined for manufactured solution
  FunctionPtr u_bc;
  FunctionPtr forcing;
  
  // set up a parameter function; this will allow us to have an exact solution that is time-dependent
  ParameterFunctionPtr t = ParameterFunction::parameterFunction(0.0);
  
  if (problemChoice == SINUSOIDAL_MANUFACTURED)
  {
    FunctionPtr x = Function::xn(1);
    FunctionPtr y = Function::yn(1);
    FunctionPtr exp_decay = Exp<double>::exp(-PI*PI*FunctionPtr(t));
    u_exact = exp_decay * TrigFunctions<double>::sin(PI * x) * TrigFunctions<double>::sin(PI * y);
    u_bc = u_exact; // zero on boundary of unit square
    forcing = PI*PI*u_exact;
  }
  else // UNIT_FORCING
  {
    u_bc = Function::zero();
    forcing = Function::constant(1.0);
  }
  
  // Test norm
  IPPtr ip = IP::ip();
  
  // BC
  BCPtr bc = BC::bc();
  
  switch (formulationChoice) {
    case GALERKIN:
      bc->addDirichlet(form.u(), SpatialFilter::allSpace(), u_bc);
      deltaP = 0;
      ip = Teuchos::null;
      break;
    case PRIMAL:
      bc->addDirichlet(form.u(), SpatialFilter::allSpace(), u_bc);
      ip = bf->naiveNorm(spaceDim); // Mathematician's norm
      // TODO: implement test norm so that the energy norm is the same as the norm that Galerkin converges in
      if (rank==0) cout << "PRIMAL: Using Mathematician's norm." << endl;
      break;
    case CONFORMING_ULTRAWEAK ... NONCONFORMING_ULTRAWEAK:
      bc->addDirichlet(form.u_hat(), SpatialFilter::allSpace(), u_bc);
      if (normChoice == MANUAL_GRAPH_NORM) {
        if (rank==0) cout << "ULTRAWEAK: Using manual graph norm." << endl;
        // True adjoint graph norm (not scaled):
        // this norm avoids the addition of L^2 terms; we still have a localizable norm in this case
//        ip->addTerm(tau->div() + v / dt);  // corresponds to u
//        ip->addTerm(tau + v->grad());      // corresponds to sigma
          
        // ToFu's norm:
        // introduces an additional 1/dt coefficient on some of the terms to eliminate cross-terms
        ip->addTerm(v/dt);
        ip->addTerm(v->grad()/sqrt(dt));
        ip->addTerm(tau/sqrt(dt));
        ip->addTerm(tau->div());
      } else {
        ip = bf->graphNorm(); // Scaled Adjoint graph norm
        if (rank==0) cout << "ULTRAWEAK: Using scaled adjoint graph norm." << endl;
      }
      break;
  }
  
  // TODO: not using domainSize yet
  double width = 1.0, height = 1.0;
  int horizontalElements = numElements, verticalElements = numElements;
  MeshTopologyPtr meshTopo = MeshFactory::quadMeshTopology(width, height, horizontalElements, verticalElements);
  MeshPtr mesh = Teuchos::rcp( new Mesh(meshTopo, bf->varFactory(), polyOrder, deltaP) );
  mesh->setBilinearForm(bf);
  
  // RHS
  RHSPtr rhs = RHS::rhs();
  rhs->addTerm(forcing * v);
  SolutionPtr soln      = Solution::solution(bf, mesh, bc, rhs, ip);
  SolutionPtr soln_prev = Solution::solution(bf, mesh, bc, rhs, ip);
  FunctionPtr u_prev    = Function::solution(u, soln_prev);
  rhs->addTerm((u_prev/dt) * v);
  
  // Initial data
  map<int, FunctionPtr> initValueMap;
  initValueMap[u->ID()] = u_exact;
  soln_prev->projectOntoMesh(initValueMap, 0);
  
  // Vis path
  ostringstream vizPath;
  vizPath << "heat_transient";
  HDF5Exporter vizExporter(mesh, vizPath.str());
  if (vis) vizExporter.exportSolution(soln_prev, t->getConstantValue());
  
  // Time stepping
  int numTimeSteps = int(finalTime / dt);
  if (rank==0) cout << "          time step  = " << dt << endl;
  if (rank==0) cout << "number of time steps = " << numTimeSteps << endl;
  
  for (int timeStepNumber = 0; timeStepNumber < numTimeSteps; timeStepNumber++)
  {
    t->setValue((timeStepNumber+1)*dt); // update forcing term, exact solution to current time step
    Epetra_Time timer(*mesh->Comm());
    soln->solve();
    double solveTime = timer.ElapsedTime();
    if (rank == 0) cout << setw(3) << timeStepNumber << " Solved in " << solveTime << " seconds.\n";
    soln_prev->setSolution(soln); // update previous solution to current time step
    if (vis) vizExporter.exportSolution(soln, t->getConstantValue());
  }
  
  if (vis && rank==0) cout << "Exported solution to " << vizPath.str() << endl;
  
  // Compute best approximation and display error
  if (problemChoice == SINUSOIDAL_MANUFACTURED)
  {
    // Compute best approximation (L2 projection)
    SolutionPtr bestApproximation = Solution::solution(bf, mesh, bc, rhs, ip);
    int solutionOrdinal = 0;
    map<int, FunctionPtr> exactSolutionMap;
    FunctionPtr sigma_exact = u_exact->grad();
    FunctionPtr n = Function::normal();
    FunctionPtr sgn = Function::sideParity();
    exactSolutionMap[form.u()->ID()] = u_exact;
    if (choice == HeatFormulation::PRIMAL) {
      exactSolutionMap[form.sigma_n_hat()->ID()] = sigma_exact * n * sgn;
    }
    if (choice == HeatFormulation::ULTRAWEAK) {
      exactSolutionMap[form.u_hat()->ID()] = u_exact;
      exactSolutionMap[form.sigma()->ID()] = sigma_exact;
      exactSolutionMap[form.sigma_n_hat()->ID()] = sigma_exact * n * sgn;
    }
    bestApproximation->projectOntoMesh(exactSolutionMap, solutionOrdinal);
    
    // Compute error for each variable
    int cubatureEnrichment = 4;
    map<int,VarPtr> trialVars = bf->varFactory()->trialVars();
    if (rank==0) cout << "Number of trial vars: " << bf->varFactory()->trialVars().size() << endl;
    double l2NormSquared = 0.0;
    double h1NormSquared = 0.0;
    //double bestDifferenceL2Squared = 0.0;
    for (auto entry : trialVars)
    {
      auto var = entry.second;
      // skip fluxes (they need minimum energy extension norm)
      if (var->varType() != FIELD) {
        continue;
      }
      auto var_exact = exactSolutionMap[var->ID()];
      
      bool weightFluxesByParity = false; // here, interested in comparing to exact solutions; should not weight
      auto var_soln = Function::solution(var, soln,              weightFluxesByParity);
      auto var_best = Function::solution(var, bestApproximation, weightFluxesByParity);
      
      double l2bestError   = (var_best - var_exact)->l2norm(mesh, cubatureEnrichment);
      double l2actualError = (var_soln - var_exact)->l2norm(mesh, cubatureEnrichment);
      double h1bestError = 0.0, h1actualError = 0.0;
      if (choice != HeatFormulation::ULTRAWEAK) {
        h1bestError   = (var_best - var_exact)->grad(spaceDim)->l2norm(mesh, cubatureEnrichment); // Error in H1 seminorm
        h1actualError = (var_soln - var_exact)->grad(spaceDim)->l2norm(mesh, cubatureEnrichment); // Error in H1 seminorm
      }
      
      double val = 1.0;
      if (choice == HeatFormulation::ULTRAWEAK) {
        if (var->ID() == form.sigma()->ID()) val = sqrt(dt);
      }
      cout << scientific;
      if (rank==0) cout << setw(18) << var->name() << " L2 BAE  : " << val*l2bestError   << endl;
      if (rank==0) cout << setw(18) << var->name() << " L2 error: " << val*l2actualError << endl;
      if (choice != HeatFormulation::ULTRAWEAK && var->ID() == form.u()->ID()) {
        if (rank==0) cout << setw(18) << var->name() << " H1 BAE  : " << h1bestError   << endl;
        if (rank==0) cout << setw(18) << var->name() << " H1 error: " << h1actualError << endl;
      }
      
      //double diffOfBest = (var_best - var_soln)->l2norm(mesh, cubatureEnrichment);
      //cout << "L^2 norm of difference between solution and best approximation: " << diffOfBest << endl;
      
      //bestDifferenceL2Squared += diffOfBest * diffOfBest;
      l2NormSquared += (val*l2actualError) * (val*l2actualError);
      h1NormSquared += h1actualError * h1actualError;
    }
    
    //double energyError = soln->energyErrorTotal();
    cout << scientific;
    if (rank==0) cout << "Error       :  " << sqrt(l2NormSquared+h1NormSquared) << endl;
    //if (rank==0) cout << "Energy error:  " << energyError << endl;
    //if (rank==0) cout << "L^2 error   :  " << sqrt(l2NormSquared) << endl;
    //if (rank==0) cout << "H^1 error   :  " << sqrt(h1NormSquared) << endl;
    //if (rank==0) cout << "L^2 diff of best: " << sqrt(bestDifferenceL2Squared) << endl;
  }
}

int main(int argc, char *argv[])
{
  Teuchos::GlobalMPISession mpiSession(&argc, &argv, NULL); // initialize MPI
  
  Teuchos::CommandLineProcessor cmdp(false,true); // false: don't throw exceptions; true: do return errors for unrecognized options
  
  int spaceDim = 2;
  int numElements = 4; // in each dimension
  vector<vector<double>> domainDim(spaceDim,vector<double>{0.0,1.0}); // first index: spaceDim; second: 0/1 for x0, x1, etc.
  int polyOrder = 3;
  int deltaP = spaceDim;
  string normChoiceString = "Trace"; // "Graph" is the alternative
  string formulationChoiceString = "Nonconforming"; // "Conforming" the alternative
  string problemChoiceString = "Manufactured"; // "Unit Forcing" the alternative
  double dt = sqrt(1.0/numElements)/20.0; // Follows Führer and Heuer: dt = sqrt(h)/20, unit square
  double finalTime = 0.1; // Follows Führer and Heuer: T = 0.1
  bool vis = false;
  bool runAll = false;
  
  cmdp.setOption("norm", &normChoiceString);
  cmdp.setOption("formulation", &formulationChoiceString);
  cmdp.setOption("problem", &problemChoiceString);
  cmdp.setOption("meshWidth", &numElements );
  cmdp.setOption("polyOrder", &polyOrder );
  cmdp.setOption("deltaP", &deltaP);
  cmdp.setOption("dt", &dt);
  cmdp.setOption("T", &finalTime);
  cmdp.setOption("vis", "novis", &vis);
  cmdp.setOption("runAll", "runOne", &runAll);
  cmdp.setOption("x0", &domainDim[0][0] );
  cmdp.setOption("x1", &domainDim[0][1] );
  cmdp.setOption("y0", &domainDim[1][0] );
  cmdp.setOption("y1", &domainDim[1][1] );
  
  if (cmdp.parse(argc,argv) != Teuchos::CommandLineProcessor::PARSE_SUCCESSFUL)
  {
#ifdef HAVE_MPI
    MPI_Finalize();
#endif
    return -1;
  }
  dt = sqrt(1.0/numElements)/20.0;
  
  NormChoice normChoice;
  if      (normChoiceString == "ManualGraph") normChoice = MANUAL_GRAPH_NORM;
  else if (normChoiceString == "Graph"      ) normChoice = GRAPH_NORM;
  else
  {
    TEUCHOS_TEST_FOR_EXCEPTION(true, std::invalid_argument, "unrecognized norm choice");
  }
  
  FormulationChoice formulationChoice;
  if      (formulationChoiceString == "NonconformingUW") formulationChoice = NONCONFORMING_ULTRAWEAK;
  else if (formulationChoiceString == "ConformingUW"   ) formulationChoice = CONFORMING_ULTRAWEAK;
  else if (formulationChoiceString == "Primal"         ) formulationChoice = PRIMAL;
  else if (formulationChoiceString == "Galerkin"       ) formulationChoice = GALERKIN;
  else
  {
    TEUCHOS_TEST_FOR_EXCEPTION(true, std::invalid_argument, "unrecognized formulation choice");
  }
  
  ProblemChoice problemChoice;
  if (problemChoiceString == "Manufactured") problemChoice = SINUSOIDAL_MANUFACTURED;
  else
  {
    TEUCHOS_TEST_FOR_EXCEPTION(true, std::invalid_argument, "unrecognized problem choice");
  }

  if (!runAll) {
    runSolve(domainDim, numElements, dt, finalTime, polyOrder, deltaP, problemChoice, formulationChoice, normChoice, vis);
  }
  else {
    vector<int> elementCounts = {1,2,4,8,16,32}; // should update dt correspondingly
    vector<NormChoice> normChoices = {GRAPH_NORM, MANUAL_GRAPH_NORM};
    for (int numElements : elementCounts)
    {
      cout << "*************** mesh width " << numElements << " *******************\n";
      for (NormChoice normChoice : normChoices)
      {
        runSolve(domainDim, numElements, dt, finalTime, polyOrder, deltaP, problemChoice, formulationChoice, normChoice, vis);
      }
    }
  }
  
  return 0;
}
